package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;

public class Edge<G extends NodeInterface> {
	public final G from, to;
	private int arr_size;
	// for arrows, using the smallest angle in (5,12,13)-triangle
	private static final double arr_cos = 12.0/13.0;
	private static final double arr_sin =  5.0/13.0;

	private static final double control_point_tangent = 1.0/8.0;

	public Edge(G v1, G v2) {
		from = v1; to = v2;
		arr_size = 10;
	}

	public Edge(int arr_size, G v1, G v2) {
		from = v1; to = v2;
		this.arr_size = arr_size;
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof Edge && from.equals(((Edge<NodeInterface>)o).from) && to.equals(((Edge<NodeInterface>)o).to));
	}

	public boolean equals(Edge<G> o) {
		return from.equals(o.from) && to.equals(o.to);
	}

	@Override
	public int hashCode() {
		return from.hashCode()+to.hashCode();
	}

	public boolean contains(NodeInterface v) {
		return from.equals(v) || to.equals(v);
	}

	public void drawEdge(AbstractCanvas canvas, boolean bothside) {
		if(bothside) {
			drawCurvedEdge(canvas, control_point_tangent);
		}
		else {
			drawStraightEdge(canvas);
		}
	}

	public void drawStraightEdge(AbstractCanvas canvas) {
		Point p1 = from.getEdgePoint(to);
		Point p2 = to.getEdgePoint(from);
		if(to.inside(p1) || from.inside(p2)) return;
		// arrow
		double cosine = p1.x - p2.x;
		double sine = p1.y - p2.y;
		double d  = Math.sqrt(cosine * cosine + sine * sine);
		cosine /= d;
		sine /= d;
		// straight line
		canvas.drawLine((int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y);

		if(arr_size > 0) {
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos-sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos+cosine*arr_sin)));
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos+sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos-cosine*arr_sin)));
		}
	}

	public void drawSlidedEdge(AbstractCanvas canvas, double tan) {
		Point p1 = from.getPoint();
		Point p2 = to.getPoint();
		double cosine = p1.x - p2.x;
		double sine = p1.y - p2.y;
		double d  = Math.sqrt(cosine * cosine + sine * sine);
		cosine /= d;
		sine /= d;
		// slided line
		// edge points with from-p1 line and p1-to line are connected
		Point t = new Point((int)((p1.x+p2.x)/2-sine*d*tan), (int)((p1.y+p2.y)/2+cosine*d*tan));

		p2 = to.getEdgePoint(t);
		p1 = from.getEdgePoint(t);
		if(to.inside(p1) || from.inside(p2)) return;

		canvas.drawLine((int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y);

		if(arr_size > 0) {
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos-sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos+cosine*arr_sin)));
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos+sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos-cosine*arr_sin)));
		}
}

	public void drawCurvedEdge(AbstractCanvas canvas, double tan) {
		Point t = from.getPoint();
		Point p2 = to.getPoint();
		double cosine = t.x - p2.x;
		double sine = t.y - p2.y;
		double d  = Math.sqrt(cosine * cosine + sine * sine);
		cosine /= d;
		sine /= d;
		// curved line
		// control point of bezier-curve
		Point p1 = new Point((int)((t.x+p2.x)/2-sine*d*tan), (int)((t.y+p2.y)/2+cosine*d*tan));

		p2 = to.getEdgePoint(p1);
		t = from.getEdgePoint(p1);
		if(to.inside(t) || from.inside(p2)) return;

		canvas.drawQuadCurve(t.x, t.y, p1.x, p1.y, p2.x, p2.y);

		cosine = p1.x - p2.x;
		sine = p1.y - p2.y;
		d  = Math.sqrt(cosine * cosine + sine * sine);
		cosine /= d;
		sine /= d;

		if(arr_size > 0) {
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos-sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos+cosine*arr_sin)));
			canvas.drawLine((int)p2.x, (int)p2.y, (int)(p2.x+arr_size*(cosine*arr_cos+sine*arr_sin)), (int)(p2.y+arr_size*(sine*arr_cos-cosine*arr_sin)));
		}
	}
}
