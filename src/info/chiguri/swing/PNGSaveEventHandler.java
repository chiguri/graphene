package info.chiguri.swing;

import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class PNGSaveEventHandler implements Consumer<MouseEvent> {
	protected JComponent renderer;

	public PNGSaveEventHandler(JComponent j) {
		renderer = j;
	}

	protected void saveImage(String filename, BufferedImage img) {
		try {
			filename += ".png";
			ImageIO.write(img, "png", new File(filename));
			System.out.println("Save Image as " + filename);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void accept(MouseEvent e) {
		BufferedImage img = new BufferedImage(renderer.getWidth(), renderer.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		renderer.paint(img.createGraphics());
		String filename = Long.toString(System.currentTimeMillis());
		saveImage(filename, img);
	}
}
