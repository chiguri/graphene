package info.chiguri.graphene.graph;

public class NonPlanarGraphException extends RuntimeException {
	public <G extends NodeInterface, E extends Edge<G>> NonPlanarGraphException(Graph<G, E> g) {
		super(g.toString() + " is not a planar graph"); // maybe unreadable message (because toString method is inherited from Object)
	}
}
