package info.chiguri.graphene.canvas;

public abstract class AbstractFont {

	abstract public AbstractFont deriveFont(float fsize);

}
