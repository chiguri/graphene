package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.canvas.AbstractFont;

public class CircleNumberNode extends CircleNode {
	public final int number;
	protected int fsize;

	public CircleNumberNode(int number, int r, int fsize, Point p) {
		super(r, p);
		this.number = number;
		this.fsize = fsize;
	}
	public CircleNumberNode(int number, int r, int fsize, int x, int y) {
		super(r, x, y);
		this.number = number;
		this.fsize = fsize;
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		super.draw(canvas);
		AbstractFont original = canvas.getFont();
		canvas.setFont(original.deriveFont((float)fsize));
		canvas.drawString(""+number, (int)(p.x)-r/2, (int)(p.y)+r/2); // TODO position adjustment is necessary
		canvas.setFont(original);
	}

}
