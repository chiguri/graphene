package info.chiguri.graphene.argumentation;

public enum Label {
	in,
	out,
	undec
}
