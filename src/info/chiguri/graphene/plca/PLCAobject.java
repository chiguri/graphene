package info.chiguri.graphene.plca;

public interface PLCAobject {

	public void focused();

	public void unfocused();
}
