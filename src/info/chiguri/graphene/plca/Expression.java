package info.chiguri.graphene.plca;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.chiguri.graphene.graph.Graph;

public class Expression {
	SortedMap<Integer, Point>   P = new TreeMap<>();
	SortedMap<Integer, Line>    L = new TreeMap<>();
	SortedMap<Integer, Circuit> C = new TreeMap<>();
	SortedMap<Integer, Area>    A = new TreeMap<>();
	Circuit   o;
	protected HashMap<Line, Line> inverse = new HashMap<>();
	protected HashMap<Point,   Integer> P_ = new HashMap<>();
	protected HashMap<Line,    Integer> L_ = new HashMap<>();
	protected HashMap<Circuit, Integer> C_ = new HashMap<>();
	protected HashMap<Area,    Integer> A_ = new HashMap<>();

	protected int max_pid = -1;
	protected int max_lid = -1;
	protected int max_cid = -1;
	protected int max_aid = -1;
	final Graph<Point, Line> g = new Graph<>(); // 描画のため

	private static Pattern pointstrpattern = Pattern.compile("P(\\p{Digit}+):\\((\\p{Digit}+),(\\p{Digit}+)\\)");
	private static Pattern linestrpattern = Pattern.compile("L(\\p{Digit}+):P(\\p{Digit}+)--P(\\p{Digit}+)");
	private static Pattern circuitstrpattern = Pattern.compile("C(\\p{Digit}+):L(\\p{Digit}+(->L\\p{Digit}+)+)");
	private static Pattern areastrpattern = Pattern.compile("A(\\p{Digit}+):C(\\p{Digit}+(,C\\p{Digit}+)*)");
	private static Pattern trimpattern = Pattern.compile(" \t\r\n");

	private void putPoint(Integer i, Point p) {
		P.put(i, p);
		P_.put(p, i);

		g.addNode(p);
	}
	private void putLine(Integer i, Line l, Line inv) {
		L.put(i, l);
		L_.put(l, i);
		L_.put(inv, i);
		inverse.put(l, inv);
		inverse.put(inv, l);

		g.addEdge(l);
		g.addEdge(inv);
	}
	private void putCircuit(Integer i, Circuit c) {
		C.put(i, c);
		C_.put(c, i);
	}
	private void putArea(Integer i, Area a) {
		A.put(i, a);
		A_.put(a, i);
	}


	public Point newPoint(int x, int y) {
		Integer id = ++max_pid;
		Point p = new Point(x, y);
		putPoint(id, p);
		return p;
	}

	public Point newPoint(Integer id, int x, int y) {
		if(P.containsKey(id)) {
			throw new RuntimeException("Point ID " + id + " already exists");
		}
		if(id > max_pid) {
			max_pid = id;
		}
		Point p = new Point(x, y);
		putPoint(id, p);
		return p;
	}

	public Line newLine(Point from, Point to) {
		Integer id = ++max_lid;
		Line l = new Line(from, to);
		Line inv = new Line(to, from);
		putLine(id, l, inv);
		return l;
	}

	public Line newLine(Integer id, Point from, Point to) {
		if(L.containsKey(id)) {
			throw new RuntimeException("Line ID " + id + " already exists");
		}
		if(id > max_lid) {
			max_lid = id;
		}
		Line l = new Line(from, to);
		Line inv = new Line(to, from);
		putLine(id, l, inv);
		return l;
	}

	public Circuit newCircuit(Line[] lines) {
		Integer id = ++max_cid;
		Circuit c = new Circuit(lines);
		putCircuit(id, c);
		return c;
	}

	public Circuit newCircuit(Integer id, Line[] lines) {
		if(C.containsKey(id)) {
			throw new RuntimeException("Circuit ID " + id + " already exists");
		}
		if(id > max_cid) {
			max_cid = id;
		}
		Circuit c = new Circuit(lines);
		putCircuit(id, c);
		return c;
	}

	public Area newArea(Circuit c) {
		Integer id = ++max_aid;
		Area a = new Area(c);
		putArea(id, a);
		return a;
	}

	public Area newArea(Circuit[] cs) {
		Integer id = ++max_aid;
		Area a = new Area(cs);
		putArea(id, a);
		return a;
	}

	public Area newArea(Integer id, Circuit c) {
		if(A.containsKey(id)) {
			throw new RuntimeException("Area ID " + id + " already exists");
		}
		if(id > max_aid) {
			max_aid = id;
		}
		Area a = new Area(c);
		putArea(id, a);
		return a;
	}

	public Area newArea(Integer id, Circuit[] cs) {
		if(A.containsKey(id)) {
			throw new RuntimeException("Area ID " + id + " already exists");
		}
		if(id > max_aid) {
			max_aid = id;
		}
		Area a = new Area(cs);
		putArea(id, a);
		return a;
	}

	public void checkInverse(Line[] seg) {
		Point next = seg[0].to;
		if(next != seg[1].from && next != seg[1].to) {
			next = seg[0].from;
			seg[0] = inverse.get(seg[0]);
		}
		for(int i = 1; i < seg.length; ++i) {
			if(next != seg[i].from) {
				seg[i] = inverse.get(seg[i]);
			}
			next = seg[i].to;
		}
	}

	public Point parsePoint(String str) {
		Matcher result = pointstrpattern.matcher(str);
		if(result.matches()) {
			Integer id = Integer.parseInt(result.group(1));
			int x = Integer.parseInt(result.group(2));
			int y = Integer.parseInt(result.group(3));
			return newPoint(id, x, y);
		}
		throw new IllegalArgumentException(str + " do not match Point pattern");
	}

	public Line parseLine(String str) {
		Matcher result = linestrpattern.matcher(str);
		if(result.matches()) {
			Integer id = Integer.parseInt(result.group(1));
			Point from = P.get(Integer.parseInt(result.group(2)));
			if(from == null) {
				throw new RuntimeException(result.group(1) + " is not generated");
			}
			Point to = P.get(Integer.parseInt(result.group(3)));
			if(to == null) {
				throw new RuntimeException(result.group(2) + " is not generated");
			}
			return newLine(id, from, to);
		}
		throw new IllegalArgumentException(str + " do not match Line pattern");
	}

	public Circuit parseCircuit(String str) {
		Matcher result = circuitstrpattern.matcher(str);
		if(result.matches()) {
			Integer id = Integer.parseInt(result.group(1));
			String[] strs = result.group(2).split("->L");
			Line[] ls = new Line[strs.length];
			for(int i = 0; i < ls.length; ++i) {
				ls[i] = L.get(Integer.parseInt(strs[i]));
				if(ls[i] == null) {
					throw new RuntimeException("Line ID " + strs[i] + " does not exist");
				}
			}
			// 正しい有向辺を選択する（番号は無向辺なので）
			checkInverse(ls);

			return newCircuit(id, ls);
		}
		throw new IllegalArgumentException(str + " do not match Circuit pattern");
	}

	public Area parseArea(String str) {
		Matcher result = areastrpattern.matcher(str);
		if(result.matches()) {
			Integer id = Integer.parseInt(result.group(1));
			String[] strs = result.group(2).split(",C");
			Circuit[] cs = new Circuit[strs.length];
			for(int i = 0; i < cs.length; ++i) {
				cs[i] = C.get(Integer.parseInt(strs[i]));
				if(cs[i] == null) {
					throw new RuntimeException("Circuit ID " + strs[i] + " does not exist");
				}
			}
			return newArea(id, cs);
		}
		throw new IllegalArgumentException(str + " do not match Area pattern");
	}

	public Expression(int n) {
		if(n < 3) {
			throw new RuntimeException("Cannot initialize with less than three points");
		}
		Point[] ps = new Point[n];

		for(int i = 0; i < n; ++i) {
			int x, y; // how do you place them?
			x = y = 100;
			ps[i] = newPoint(x, y);
		}

		Line[] outer = new Line[n];
		Line[] inner = new Line[n];
		for(int i = 0; i < n; ++i) {
			outer[i] = newLine(ps[i], ps[(i+1)%n]);
			inner[n-i-1] = inverse.get(outer[i]);
		}
		o = newCircuit(outer);
		Circuit in = newCircuit(inner);
		newArea(in);
	}

	private void initWithStrings(String[] strs) {
		for(int i = 0; i < strs.length; ++i) {
			strs[i] = trimpattern.matcher(strs[i]).replaceAll("");
		}
		int i = 0;
		while(pointstrpattern.matcher(strs[i]).matches()) {
			parsePoint(strs[i]);
			if(++i >= strs.length) {
				throw new RuntimeException("No lines occur");
			}
		}
		System.out.println(i);
		while(linestrpattern.matcher(strs[i]).matches()) {
			parseLine(strs[i]);
			if(++i >= strs.length) {
				throw new RuntimeException("No circuits occur");
			}
		}
		System.out.println(i);
		while(circuitstrpattern.matcher(strs[i]).matches()) {
			parseCircuit(strs[i]);
			if(++i >= strs.length) {
				throw new RuntimeException("No areas occur");
			}
		}
		System.out.println(i);
		while(areastrpattern.matcher(strs[i]).matches()) {
			parseArea(strs[i]);
			if(++i >= strs.length) {
				throw new RuntimeException("Outermost does not occur");
			}
		}
		System.out.println(i);
		if(i < strs.length && Pattern.matches("C\\p{Digit}+", strs[i])) {
			o = C.get(Integer.parseInt(strs[i].substring(1)));
			if(o == null) {
				throw new RuntimeException("Circuit ID " + strs[i].substring(1) + " does not exist");
			}
		}
		else {
			throw new RuntimeException("Outermost does not occur");
		}
	}

	public Expression(String str) {
		initWithStrings(str.split("[\r\n]+")); // \nだけでよい？
	}

	public Expression(String[] strs) {
		initWithStrings(strs);
	}

	Expression() {
		// 何もしないので気をつけること
	}

	void addPoint(Point p) {
		Integer id = ++max_pid;
		putPoint(id, p);
	}

	void addLine(Line l, Line inv) {
		Integer id = ++max_lid;
		putLine(id, l, inv);
	}

	void setOutermost(Circuit o) {
		this.o = o;
	}

	void addCircuit(Circuit c) {
		Integer id = ++max_cid;
		putCircuit(id, c);
	}

	void addArea(Area a) {
		Integer id = ++max_aid;
		putArea(id, a);
	}

	// outermostから順に内に向かって番号を付けていく
	public void renumber() {
		HashMap<Line, Circuit> lc = new HashMap<>();
		HashMap<Circuit, Area> ca = new HashMap<>();
		for(Area a : A.values()) {
			for(Circuit c : a) {
				ca.put(c, a);
				for(Line l : c.seg) {
					lc.put(l, c);
				}
			}
		}

		HashSet<Area> aa = new HashSet<>();
		HashSet<Circuit> cc = new HashSet<>();
		HashSet<Line> ll = new HashSet<>();
		HashSet<Point> pp = new HashSet<>();
		P = new TreeMap<>();
		L = new TreeMap<>();
		C = new TreeMap<>();
		A = new TreeMap<>();

		P_ = new HashMap<>();
		L_ = new HashMap<>();
		C_ = new HashMap<>();
		A_ = new HashMap<>();

		max_pid = max_lid = max_cid = max_aid = -1;
		LinkedList<Circuit> rest = new LinkedList<>();
		LinkedList<Area> rest_a = new LinkedList<>();
		rest.add(o);
		while(!rest.isEmpty()) {
			while(!rest.isEmpty()) {
				Circuit c = rest.poll();
				putCircuit(++max_cid, c);
				// cの属するareaは処理済みか
				Area a = ca.get(c);
				if(a != null && aa.add(a)) { // 処理されていないのでareaの処理待ちキューに入れる
					rest_a.add(a);
				}
				for(Line l : c.seg) {
					Point p = l.from;
					if(pp.add(p)) {
						putPoint(++max_pid, p);
					}
					if(ll.add(l)) {
						Line inv = inverse.get(l);
						ll.add(inv);
						putLine(++max_lid, l, inv);
						Circuit cl = lc.get(inv);
						if(cc.add(cl)) {
							rest.add(cl);
						}
					}

				}
			}
			while(!rest_a.isEmpty() && rest.isEmpty()) {
				Area a = rest_a.poll();
				putArea(++max_aid, a);
				for(Circuit cx : a) {
					if(cc.add(cx)) {
						rest.add(cx);
					}
				}
			}
		}
	}


	public String toString(Point p) {
		return "P" + P_.get(p);
	}
	public String toFullString(Point p) {
		return toString(p)+":("+(int)p.getPoint().x+","+(int)p.getPoint().y+")";
	}
	public String toString(Line l) {
		return "L" + L_.get(l);
	}
	public String toFullString(Line l) {
		return toString(l)+":"+toString(l.from)+"--"+toString(l.to);
	}
	public String toString(Circuit c) {
		return "C" + C_.get(c);
	}
	public String toFullString(Circuit c) {
		Iterator<Line> s = c.segmentIterator();
		String str = toString(c) + ":" + toString(s.next());
		while(s.hasNext()) {
			str += "->" + toString(s.next());
		}
		return str;
	}
	public String toString(Area a) {
		return "A" + A_.get(a);
	}
	public String toFullString(Area a) {
		Iterator<Circuit> s = a.iterator();
		String str = toString(a) + ":" + toString(s.next());
		while(s.hasNext()) {
			str += "," + toString(s.next());
		}
		return str;
	}
	public void printPLCA(PrintStream out) {
		for(Point p : P.values()) {
			out.println(toFullString(p));
		}
		for(Line l : L.values()) {
			out.println(toFullString(l));
		}
		for(Circuit c : C.values()) {
			out.println(toFullString(c));
		}
		for(Area a : A.values()) {
			out.println(toFullString(a));
		}
		out.println(toString(o));
	}

	public void clearFocused() {
		for(Point p : P.values()) {
			p.unfocused();
		}
		for(Line l : inverse.values()) {
			l.unfocused();
		}
	}

	public Line getInverse(Line l) {
		return inverse.get(l);
	}

	public Collection<Point> getPoints() {
		return P.values();
	}

	public Collection<Line> getLines() {
		return L.values();
	}

	public Collection<Circuit> getCircuits() {
		return C.values();
	}

	public Collection<Area> getAreas() {
		return A.values();
	}

	public Circuit getO() {
		return o;
	}
}
