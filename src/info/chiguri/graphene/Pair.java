package info.chiguri.graphene;

public class Pair<T1, T2> {
	public final T1 fst;
	public final T2 snd;
	public Pair(T1 f, T2 s) {
		fst = f;
		snd = s;
	}
}
