package info.chiguri.swing.canvas;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.QuadCurve2D;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.canvas.AbstractColor;
import info.chiguri.graphene.canvas.AbstractFont;

public class SwingCanvas extends AbstractCanvas {
	protected final Graphics2D g;
	protected SwingColor currentColor;
	protected SwingFont currentFont;
	protected float stroke;

	public SwingCanvas(Graphics2D g) {
		this.g = g;
		stroke = 1.0f; // default in Java
		g.setStroke(new BasicStroke(1.0f));
		currentColor = new SwingColor(g.getColor());
		currentFont = new SwingFont(g.getFont());
	}

	@Override
	public void drawLine(int x1, int y1, int x2, int y2) {
		g.drawLine(x1, y1, x2, y2);
	}

	@Override
	public void drawRect(int x, int y, int width, int height) {
		g.drawRect(x, y, width, height);
	}

	@Override
	public void fillRect(int x, int y, int width, int height) {
		g.fillRect(x, y, width, height);
	}

	@Override
	public void drawOval(int x, int y, int width, int height) {
		g.drawOval(x, y, width, height);
	}

	@Override
	public void fillOval(int x, int y, int width, int height) {
		g.fillOval(x, y, width, height);
	}

	@Override
	public AbstractColor getColor() {
		return currentColor;
	}

	@Override
	public void setColor(AbstractColor c) {
		SwingColor col = (SwingColor)c;
		g.setColor(col.c);
		currentColor = col;
	}

	@Override
	public AbstractFont getFont() {
		return currentFont;
	}

	@Override
	public void setFont(AbstractFont f) {
		SwingFont font = (SwingFont)f;
		g.setFont(font.f);
		currentFont = font;
	}

	@Override
	public void drawString(String str, int x, int y) {
		g.drawString(str, x, y);

	}

	@Override
	public void drawQuadCurve(int start_x, int start_y, double control_x, double control_y, double end_x,	double end_y) {
		QuadCurve2D.Double edge = new QuadCurve2D.Double(start_x, start_y, control_x, control_y, end_x, end_y);
		g.draw(edge);
	}

	@Override
	public void drawCubicCurve(int start_x, int start_y, double control_x1, double control_y1, double control_x2, double control_y2, double end_x,	double end_y) {
		CubicCurve2D.Double edge = new CubicCurve2D.Double(start_x, start_y, control_x1, control_y1, control_x2, control_y2, end_x, end_y);
		g.draw(edge);
	}

	@Override
	public void setAntiAlias(boolean flag) {
		if(flag) {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
			                   RenderingHints.VALUE_ANTIALIAS_ON);
		}
		else {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	                   RenderingHints.VALUE_ANTIALIAS_OFF);
		}
	}

	@Override
	public float getStrokeWidth() {
		return stroke;
	}

	@Override
	public void setStrokeWidth(float width) {
		stroke = width;
		g.setStroke(new BasicStroke(stroke));
	}
}
