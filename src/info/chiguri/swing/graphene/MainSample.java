package info.chiguri.swing.graphene;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Function;

import javax.swing.JFrame;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.graph.CircleNumberNode;
import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.Point;
import info.chiguri.swing.LeftDoubleClickedMouse;

public class MainSample {
	static private Graph<CircleNumberNode, Edge<CircleNumberNode>> g;
	// 見た目パラメータ
	static final int RIGHT = 20, LEFT = 680, TOP = 20, BOTTOM = 680;
	static final int NODESIZE = 30;
	static public final Function<Point, CircleNumberNode> generator = new Function<Point, CircleNumberNode>() {
		int i = 0;
		@Override
		public CircleNumberNode apply(Point p) {
			return new CircleNumberNode(++i, NODESIZE, NODESIZE, p);
		}
	};


	public static void readGraph(InputStream input) {
		g = new Graph<CircleNumberNode, Edge<CircleNumberNode>>();
		BufferedReader in = new BufferedReader(new InputStreamReader(input));
		String str = "";
		int num = -1;
		int i = 0;
		int linenum = 0;
		try {
			++linenum;
			str = in.readLine();
			num = Integer.valueOf(str);
			if(num < 0) {
				System.err.println("Number of nodes should be non-negative number");
				System.exit(1);
			}
			CircleNumberNode[] nodes = new CircleNumberNode[num];
			for(i = 0; i < num; ++i) {
				++linenum;
				str = in.readLine();
				String [] strs = str.split(" ");
				if(strs.length < 2) {
					throw new Exception();
				}
				int x = Integer.valueOf(strs[0]);
				int y = Integer.valueOf(strs[1]);
				nodes[i] = generator.apply(new Point(x, y));
				g.addNode(nodes[i]);
			}

			++linenum;
			str = in.readLine(); // 一行読み飛ばし

			// edgeの書式どうするかなあ・・・
			int edgenum = Integer.valueOf(str);

			for(i = 0; i < edgenum; ++i) {
				++linenum;
				str = in.readLine();
				String [] strs = str.split(" ");
				if(strs.length < 2) {
					throw new Exception();
				}
				int x = Integer.valueOf(strs[0]);
				int y = Integer.valueOf(strs[1]);
				if(x <= 0 || x > num) {
					System.err.println("Node number is out of range : " + x);
					System.exit(1);
				}
				if(y <= 0 || y > num) {
					System.err.println("Node number is out of range : " + y);
					System.exit(1);
				}
				g.addEdge(new Edge<CircleNumberNode>(nodes[x-1], nodes[y-1]));
			}
		} catch (IOException e) {
			System.err.println("Format exception at line " + linenum + "  the last read line is : " + str);
			e.printStackTrace();
			System.exit(1);
		} catch (NumberFormatException e) {
			if(linenum <= 1) {
				System.err.println("The first line should be the number of nodes but given : " + str);
			}
			else {
				System.err.println("Pair of number required at line " + linenum + " but given : " + str);
			}
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Requires two numbers, but given : " + str);
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void printGraph(PrintStream out) {
		if(out == System.out || out == System.err) {
			out.println("The result of graph");
		}
		CircleNumberNode[] nodes = new CircleNumberNode[g.nodeNum()];
		for(CircleNumberNode v : new IterableIterator<>(g.nodeIterator())) {
			nodes[v.number-1] = v;
		}

		out.println(g.nodeNum());
		for(int i = 0; i < g.nodeNum(); ++i) {
			out.println((int)(nodes[i].getPoint().x) + " " + (int)(nodes[i].getPoint().y));
		}
		out.println(g.edgeNum());
		for(Edge<CircleNumberNode> e : new IterableIterator<>(g.edgeIterator())) {
			out.println(e.from.number + " " + e.to.number);
		}
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			g = new Graph<CircleNumberNode, Edge<CircleNumberNode>>();
		}
		else {
			if(args[0].equals("-i")) {
				readGraph(System.in);
			}
			else {
				try {
					readGraph(new FileInputStream(args[0]));
				} catch (FileNotFoundException e) {
					System.err.println("The file " + args[0] + " cannot be opened");
					e.printStackTrace();
					System.exit(1);
				}
			}
		}

		final JFrame m = new JFrame();

		m.setSize(LEFT+RIGHT, BOTTOM+TOP);
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				printGraph(System.out);
			}
		});
		GPanel<CircleNumberNode, Edge<CircleNumberNode>> panel = new GPanel<>(g);
		panel.addMouseAdapter(new LeftDoubleClickedMouse(new PNGSaveTrimmingEventHandler<CircleNumberNode, Edge<CircleNumberNode>>(panel, g, NODESIZE+2) {
			@Override
			protected void saveImage(String filename, BufferedImage img) {
				super.saveImage(filename, img);
				try {
					printGraph(new PrintStream(filename));
					System.out.println("Save Text Expression as " + filename);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}));
		panel.addMouseAdapter(new ClickedMoveMouse<>(g));
		panel.addMouseAdapter(new ModifyGraphMouse<>(g, generator, (from, to) -> { return new Edge<>(from, to); }));
		m.add(panel);
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 100, 100);
	}

}
