package info.chiguri.graphene.graph;

public interface ToggleNodeInterface extends NodeInterface {
	abstract public boolean setMode(boolean b);
}
