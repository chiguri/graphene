package info.chiguri.graphene.canvas;

public abstract class AbstractCanvas {
	abstract public void drawLine(int x1, int y1, int x2, int y2);

	abstract public void drawRect(int x, int y, int width, int height);

	abstract public void fillRect(int x, int y, int width, int height);

	abstract public void drawOval(int x, int y, int width, int height);

	abstract public void fillOval(int x, int y, int width, int height);

	abstract public AbstractColor getColor();

	abstract public void setColor(AbstractColor c);

	abstract public AbstractFont getFont();

	abstract public void setFont(AbstractFont f);

	abstract public void drawString(String str, int x, int y);

	abstract public void drawQuadCurve(int start_x, int start_y, double control_x, double control_y, double end_x, double end_y);

	abstract public void drawCubicCurve(int start_x, int start_y, double control_x1, double control_y1, double control_x2, double control_y2, double end_x, double end_y);

	abstract public void setAntiAlias(boolean flag);

	abstract public float getStrokeWidth();

	abstract public void setStrokeWidth(float width);
}
