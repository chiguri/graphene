package info.chiguri.swing;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

import javax.swing.SwingUtilities;

public class LeftDoubleClickedMouse extends MouseAdapter {
	protected Consumer<MouseEvent> consumer;
	public LeftDoubleClickedMouse(Consumer<MouseEvent> consumer) {
		this.consumer = consumer;
	}

	public void mousePressed(MouseEvent e) {
		if(e.getClickCount() == 2 && !e.isConsumed() && SwingUtilities.isLeftMouseButton(e)) {
			consumer.accept(e);
		}
	}
}
