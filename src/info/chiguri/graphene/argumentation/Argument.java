package info.chiguri.graphene.argumentation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.graph.CircleStringNode;
import info.chiguri.graphene.graph.Point;

public class Argument extends CircleStringNode {
	protected Label state = Label.undec;

	private static Pattern argumentPattern = Pattern.compile("^(.+)--\\((.+),(.+)\\)");

	public Argument(String name, int x, int y) {
		super(name, DisplayConfig.ArgumentRadius, DisplayConfig.ArgumentFontSize, x, y);
	}

	public Argument(String name, Point p) {
		super(name, DisplayConfig.ArgumentRadius, DisplayConfig.ArgumentFontSize, p);
	}

	public void setLabel(Label l) {
		if(l != null) {
			state = l;
		}
	}

	public Label getState() {
		return state;
	}

	@Override
	public void draw(AbstractCanvas g) {
		switch (state) {
		case in:
			g.setColor(DisplayConfig.InColor);
			break;

		case out:
			g.setColor(DisplayConfig.OutColor);
			break;

		case undec:
			g.setColor(DisplayConfig.UndecColor);
			break;

		default:
			throw new RuntimeException("Argument state is not initialized : " + label);
		}
		g.fillOval((int) p.x - r, (int) p.y - r, r * 2, r * 2);
		g.setColor(DisplayConfig.ArgumentEdgeColor);
		super.draw(g);
	}

	@Override
	public String toString() {
		return label;
	}

	public String toFullString() {
		return label + "--(" + p.x + "," + p.y + ")";
	}

	public boolean nameCheck(String str) {
		return str.equals(label);
	}

	public static Argument parseArgument(String str) {
		Matcher match = argumentPattern.matcher(str);
		if (match.find()) {
			String name = match.group(1);
			int x = Integer.parseInt(match.group(2));
			int y = Integer.parseInt(match.group(3));
			return new Argument(name, x, y);
		}
		return null; // throw?
	}

}
