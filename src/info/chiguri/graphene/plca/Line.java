package info.chiguri.graphene.plca;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.graph.Edge;

public class Line extends Edge<Point> implements PLCAobject {
	protected boolean selected = false;

	public Line(Point v1, Point v2) {
		super(v1, v2);
	}

	@Override
	public void drawEdge(AbstractCanvas g, boolean both) {
		float origWidth = g.getStrokeWidth();
		if(selected) {
			g.setColor(DisplayConfig.SelectedEdgeColor);
			g.setStrokeWidth(DisplayConfig.SelectedEdgeWidth);
			//g.setStroke(new BasicStroke(DisplayConfig.SelectedEdgeWidth));
		}
		else {
			g.setColor(DisplayConfig.NotSelectedEdgeColor);
		}
		super.drawSlidedEdge(g, DisplayConfig.SlidedTangent);
		g.setStrokeWidth(origWidth);
	}

	@Override
	public void focused() {
		selected = true;
		from.focused();
		to.focused();
	}

	@Override
	public void unfocused() {
		selected = false;
		from.unfocused();
		to.unfocused();
	}
}
