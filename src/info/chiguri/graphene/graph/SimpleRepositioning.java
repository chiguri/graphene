package info.chiguri.graphene.graph;

import java.util.HashMap;

import info.chiguri.graphene.IterableIterator;

public class SimpleRepositioning<G extends NodeInterface, E extends Edge<G>> extends AbstractRepositioning<G, E> {
	protected double k;
	protected double l;
	protected double c;
	protected double eps;
	final double damp;
	final double utime;
	final double mass;
	protected NodeInterface[] ns;
	protected double[] speeds_x;
	protected double[] speeds_y;
	protected double sum_ene;
	private int[] froms;
	private int[] tos;

	public SimpleRepositioning(Graph<G, E> g, double k, double l, double c, double eps, double damp, double utime, double mass) {
		super(g);
		this.k = k; this.l = l; this.c = c; this.eps = eps; this.damp = damp; this.utime = utime; this.mass = mass;
		ns = new NodeInterface[g.nodeNum()];
		{
			int i = 0;
			for(G v : new IterableIterator<>(g.nodeIterator())) {
				ns[i++] = v;
			}
		}
		speeds_x = new double[ns.length];
		speeds_y = new double[ns.length];
		sum_ene = eps;
		froms = new int[g.edgeNum()];
		tos = new int[g.edgeNum()];
		HashMap<NodeInterface, Integer> n2i = new HashMap<NodeInterface, Integer>();
		for(int i = 0; i < ns.length; ++i) {
			speeds_x[i] = 0;
			speeds_y[i] = 0;
			n2i.put(ns[i], i);
		}
		{
			int i = 0;
			for(E e : new IterableIterator<>(g.edgeIterator())) {
				froms[i] = n2i.get(e.from);
				tos[i] = n2i.get(e.to);
				++i;
			}
		}
	}
	@Override
	public boolean finish() {
		return sum_ene < eps;
	}

	//private int count = 0;

	@Override
	protected void process() {
		if(targetRevision != g.getRevision()) {
			// raise exception?
			return;
		}
		if(ns.length <= 1) { // do nothing
			return;
		}

		double[] powers_x = new double[ns.length];
		double[] powers_y = new double[ns.length];
		double[][] dists = new double[ns.length][ns.length];
		double[][] sines = new double[ns.length][ns.length];
		double[][] cosines = new double[ns.length][ns.length];

		for(int i = 0; i < ns.length; ++i) {
			powers_x[i] = 0;
			powers_y[i] = 0;

			Point p1 = ns[i].getPoint();
			for(int j = 0; j < ns.length; ++j) {
				if(i == j) continue;
				Point p2 = ns[j].getPoint();

				double x = p2.x - p1.x;
				double y = p2.y - p1.y;
				double d = x*x+y*y;
				double power = c / d;
				d = Math.sqrt(d);
				dists[i][j] = d;
				sines[i][j] = y/d;
				cosines[i][j] = x/d;

				powers_x[i] -= cosines[i][j] * power;
				powers_y[i] -=   sines[i][j] * power;
			}
		}

		for(int n = 0; n < froms.length; ++n) {
			int i = froms[n];
			int j = tos[n];
			if(i == j) continue; // self arrows are not considered as springs
			double power = k * (dists[i][j]-l);
			powers_x[i] += cosines[i][j] * power;
			powers_y[i] +=   sines[i][j] * power;
			powers_x[j] += cosines[j][i] * power;
			powers_y[j] +=   sines[j][i] * power;
		}

		sum_ene = 0;
		for(int i = 0; i < ns.length; ++i) {
			speeds_x[i] = (speeds_x[i] + utime * powers_x[i] / mass) * damp;
			speeds_y[i] = (speeds_y[i] + utime * powers_y[i] / mass) * damp;
			Point p = ns[i].getPoint();
			int x = p.x + (int)(utime * speeds_x[i]);
			int y = p.y + (int)(utime * speeds_y[i]);
			ns[i].moveTo(x, y);
			sum_ene += mass*(speeds_x[i]*speeds_x[i]+speeds_y[i]*speeds_y[i]);
		}

		//++count;
		//System.out.println(count + " : " + sum_ene);
	}

	@Override
	public void enlarge(double ratio) {
		k /= ratio;
		l *= ratio;
		c *= ratio * ratio;
		// how about eps?
		for(int i = 0; i < ns.length; ++i) {
			speeds_x[i] *= ratio;
			speeds_y[i] *= ratio;
		}
		sum_ene *= ratio * ratio;
	}
}
