package info.chiguri.graphene.argumentation;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import info.chiguri.graphene.graph.Graph;

public class ArgumentationFramework extends Graph<Argument, Attack> {
	private void updateIn(Map<Argument, Label> map, AdjacentListGraph adj, int i) {
		Label l = map.get((Argument)adj.V[i]);
		if(l == Label.undec || l == Label.out) {
			throw new UnsatisifiablePartialLabelingException(adj.V[i] + " is already labelled as \"" + l + "\" (by other part of labeling) but also designated as \"in\"");
		}
		map.put((Argument)adj.V[i], Label.in);
		for(int j : adj.E[i]) {
			l = map.get(adj.V[j]);
			if(l == Label.in || l == Label.undec) {
				throw new UnsatisifiablePartialLabelingException(adj.V[j] + " is labelled as \"" + l + "\" but attacked by " + adj.V[i] + " which is labelled as \"in\"");
			}
			map.put((Argument)adj.V[j], Label.out);
		}
	}

	// partialのラベリングを満たす極小のconsistent labelingを計算（partialがcomplete labelingの部分集合でない場合はcomplete labelingにならない）
	public Map<Argument, Label> completeLabeling(Map<Argument, Label> partial) {
		Map<Argument, Label> result = new HashMap<>();

		AdjacentListGraph adj = adjacentlist();

		for(Map.Entry<Argument, Label> e : partial.entrySet()) {
			if(e.getValue() == Label.out || e.getValue() == Label.undec) {
				result.put(e.getKey(), e.getValue());
			}
		}
		for(Map.Entry<Argument, Label> e : partial.entrySet()) {
			if(e.getValue() == Label.in) {
				updateIn(result, adj, adj.map.get(e.getKey()));
			}
		}
		boolean converge = false;

		while(!converge) {
			converge = true;
			for(int i = 0; i < adj.V.length; ++i) {
				if(!result.containsKey(adj.V[i])) {
					boolean isIn = true;
					for(int j : adj.E_r[i]) {
						Label l = result.get(adj.V[j]);
						assert l != Label.in : adj.V[j]; // なら既にV[i]はoutのハズ
						if(l == null || l == Label.undec) {
							isIn = false;
							break; // 上のassertionチェックもあるので少し危険、breakがなくても動作に支障はない（遅くなるが）
						}
					}
					if(isIn) {
						updateIn(result, adj, i);
						converge = false;
					}
				}
			}
		}
		// 後処理:つかないやつは全てundec
		for(Argument v : nodes) {
			if(!result.containsKey(v)) {
				result.put(v, Label.undec);
			}
		}
		return result;
	}

	public Map<Argument, Label> groundLabeling() {
		return completeLabeling(new HashMap<Argument, Label>());
	}

	public static ArgumentationFramework generateAF(String[] strs) {
		ArgumentationFramework AF = new ArgumentationFramework();
		HashMap<String, Argument> map = new HashMap<>();
		for(int i = 0; i < strs.length; ++i) {
			Argument a = Argument.parseArgument(strs[i]);
			if(a != null) {
				map.put(a.toString(), a);
				AF.addNode(a);
				continue;
			}
			Attack att = Attack.parseAttack(strs[i], map);
			if(att != null) {
				AF.addEdge(att);
				continue;
			}
			// 途中が解析不能ならその時点で終了（空行どうする？）
			break;
		}

		return AF;
	}

	public void printAF(PrintStream out) {
		TreeMap<String, Argument> tm = new TreeMap<>();
		for(Argument a : nodes) {
			tm.put(a.toString(), a);
		}
		for(Argument a : tm.values()) {
			out.println(a.toFullString());
		}

		TreeMap<String, Attack> tmt = new TreeMap<>();
		for(Attack att : edges) {
			tmt.put(att.toString(), att);
		}
		for(Attack att : tmt.values()) {
			out.println(att);
		}
	}
}
