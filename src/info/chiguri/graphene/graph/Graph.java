package info.chiguri.graphene.graph;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import info.chiguri.graphene.canvas.AbstractCanvas;

/**
 * @author chiguri
 *
 * @param <G>
 */
public class Graph<G extends NodeInterface, E extends Edge<G>> {
	/**
	 * revision of the graph (to detect modifying the graph)
	 */
	protected int revision = 0;
	/**
	 * Window (area in which nodes place) of the graph
	 */
	protected int left, right, top, bottom; // currently, these are saved only in normalize/random method

	/**
	 * @return current revision number of the graph
	 */
	public int getRevision() {
		return revision;
	}

	/**
	 * Set of nodes
	 */
	protected HashSet<G> nodes = new HashSet<G>();
	/**
	 * Set of edges
	 */
	protected HashSet<E> edges = new HashSet<E>();

	/**
	 * Clone this graph into a given graph.
	 * @param g : graph storing clones of this graph
	 * @param genNode : generator for cloning G
	 * @param genEdge : generator for cloning Edge<G> (first parameter fo genEdge is for mapping a node in g to new (cloned) one)
	 */
	public <T extends NodeInterface, S extends Edge<T>> void cloning(Graph<T, S> g, Function<G, T> genNode, BiFunction<Map<G, T>, E, S> genEdge) {
		if(this == g) return;
		g.clearNodes();

		HashMap<G, T> map = new HashMap<>();
		for(G v1 : nodes) {
			T v2 = genNode.apply(v1);
			g.addNode(v2);
			map.put(v1, v2);
		}

		for(E e1 : edges) {
			S e2 = genEdge.apply(map, e1);
			g.addEdge(e2);
		}
	}

	// Methods for nodes
	/**
	 * @param o : node added to this graph
	 * @return o has already stored
	 */
	public boolean addNode(G o) {
		boolean b = nodes.add(o);
		if(b) revision++;
		return b;
	}

	public boolean addAllNodes(Collection<? extends G> c) {
		boolean b = nodes.addAll(c);
		if(b) revision++;
		return nodes.addAll(c);
	}

	public boolean removeNode(G o) {
		boolean b = nodes.remove(o);
		if(b) {
			if(!removeAllEdgesWithNode(o)) {
				revision++;
			} // otherwise the method increments revision
		}
		return b;
	}

	public boolean removeAll(Collection<?> c) {
		boolean b = nodes.removeAll(c);
		if(b) {
			if(!removeEdgesIf(e->{ return c.contains(e.from) || c.contains(e.to);})) {
				revision++;
			}
		}
		return b;
	}

	public boolean removeNodesIf(Predicate<? super G> filter) {
		boolean b = nodes.removeIf(filter);
		if(b) {
			if(!removeEdgesIf(e->{ return !nodes.contains(e.from) || !nodes.contains(e.to); })) { // rebuild edges
				revision++;
			}
		}
		return b;
	}

	public void clearNodes() {
		clearEdge();
		nodes.clear();
	}

	public boolean containsNode(G e) {
		return nodes.contains(e);
	}

	public boolean containsAllNodes(Collection<?> c) {
		return nodes.containsAll(c);
	}

	public boolean equalsOnNodes(Object o) {
		return nodes.equals(o);
	}

	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	public Iterator<G> nodeIterator() {
		return nodes.iterator();
	}

	public boolean retainAllNodes(Collection<?> c) {
		boolean b = nodes.retainAll(c);
		if(b) {
			if(!removeEdgesIf(e->{ return !c.contains(e.from) || !c.contains(e.to); })) {
				revision++;
			}
		}
		return b;
	}

	/**
	 * @return number of node
	 */
	public int nodeNum() {
		return nodes.size();
	}

	// Methods for edges
	public boolean addEdge(E e) {
		boolean b = edges.add(e);
		if(b) revision++;
		return b;
	}

	public boolean addAllEdges(Collection<? extends E> c) {
		boolean b = edges.addAll(c);
		if(b) revision++;
		return b;
	}

	public boolean removeEdge(E e) {
		boolean b = edges.remove(e);
		if(b) revision++;
		return b;
	}

	public boolean removeAllEdges(Collection<?> c) {
		boolean b = edges.removeAll(c);
		if(b) revision++;
		return b;
	}

	/**
	 * Remove all edges containing the given node.
	 * @param v : node in which removed edges contain
	 * @return any edges are removed
	 */
	public boolean removeAllEdgesWithNode(G v) {
		boolean b = edges.removeIf(e->{ return e.contains(v); }); // remove all edges containing Node v
		if(b) revision++;
		return b;
	}

	public boolean removeEdgesIf(Predicate<? super E> filter) {
		boolean b = edges.removeIf(filter);
		if(b) revision++;
		return b;
	}

	public void clearEdge() {
		revision++;
		edges.clear();
	}

	public boolean containsEdge(Edge<G> e) {
		return edges.contains(e);
	}

	public boolean containsEdge(G from, G to) {
		for(E e : edges) {
			if(e.from == from && e.to == to) {
				return true;
			}
		}
		return false;
	}

	public boolean containsAllEdges(Collection<?> c) {
		return edges.containsAll(c);
	}

	public boolean equalsOnEdge(Object o) {
		return edges.equals(o);
	}

	public boolean isEdgeless() {
		return edges.isEmpty();
	}

	public Iterator<E> edgeIterator() {
		return edges.iterator();
	}

	public boolean retainAllEdges(Collection<?> c) {
		boolean b = edges.retainAll(c);
		if(b) revision++;
		return b;
	}

	public int edgeNum() {
		return edges.size();
	}

	/**
	 * Move every nodes in this graph to a random position.
	 * @param left : left-most of each position
	 * @param right : right-most of each position (left <= right)
	 * @param top : top-most of each position
	 * @param bottom : bottom-most of each position (top <= bottom)
	 */
	public void randomPositions(int left, int right, int top, int bottom) {
		this.left = left; this.right = right; this.top = top; this.bottom = bottom;
		NodeInterface[] ns = nodes.toArray(new NodeInterface[0]);
		int width = right - left;
		int height = bottom - top;
		for(int i = 0; i < ns.length; ++i) {
			boolean distinct = true;
			Point p;
			do {
				p = new Point((int)(Math.random()*width)+left, (int)(Math.random()*height)+top);
				for(int j = 0; j < i; ++j) {
					if(ns[j].getPoint().equals(p)) {
						distinct = false;
						break;
					}
				}
			} while(!distinct);
			ns[i].moveTo(p);
		}
	}

	/**
	 * Enlarge/translate nodes of graph into [left,right] and [top, bottom] window
	 * @param left
	 * @param right
	 * @param top
	 * @param bottom
	 * @return the enlargement factor of the graph
	 */
	public double normalize(int left, int right, int top, int bottom) {
		this.left = left; this.right = right; this.top = top; this.bottom = bottom;
		Iterator<G> iv = nodeIterator();
		if(!iv.hasNext()) return 1;
		Point p = iv.next().getPoint();
		double l, r, t, b;
		l = r = p.x;
		t = b = p.y;
		while(iv.hasNext()) {
			p = iv.next().getPoint();
			if(p.x < l) l = p.x;
			if(p.x > r) r = p.x;
			if(p.y < t) t = p.y;
			if(p.y > b) b = p.y;
		}
		double w_ratio = r - l > 0 ? (right - left) / (r - l) : 0;
		double h_ratio = b - t > 0 ? (bottom - top) / (b - t) : 0;
		double ratio = w_ratio < h_ratio ? w_ratio : h_ratio; // to keep aspect-ratio

		for(NodeInterface v : nodes) {
			 p = v.getPoint();
			 v.moveTo((int)((p.x-l)*ratio)+left, (int)((p.y-t)*ratio)+top);
		}

		return ratio;
	}

	public void drawWindow(AbstractCanvas canvas) {
		canvas.drawRect(left, top, right-left, bottom-top);
	}

	public void print(PrintStream out) {
		HashMap<NodeInterface, Integer> map = new HashMap<>();
		int node_num = 0;
		for(NodeInterface v : nodes) {
			map.put(v, node_num);
			++node_num;
		}
		out.println("Number of Nodes : " + node_num);
		out.println("{");
		for(E e : edges) {
			out.println("  (" + map.get(e.from) + ", " + map.get(e.to) + ")");
		}
		out.println("}");
	}


	static private int union_find_lookup(int []parents, int t) {
		int parent = parents[t];
		while(parent != parents[parent]) {
			parent = parents[parent];
		}
		return parent;
	}
	static private void union_find_flatten(int []parents, int t, int parent) {
		while(t != parent) {
			int x = parents[t];
			parents[t] = parent;
			t = x;
		}
	}
	static private void union_find_merge(int []parents, int l, int r) {
		int parent1 = union_find_lookup(parents, l);
		int parent2 = union_find_lookup(parents, r);
		if(parent1 == parent2) return;
		if(parent1 < parent2) {
			parents[parent2] = parent1;
			union_find_flatten(parents, l, parent1);
			union_find_flatten(parents, r, parent1);
		}
		else {
			parents[parent1] = parent2;
			union_find_flatten(parents, l, parent2);
			union_find_flatten(parents, r, parent2);
		}
	}

	private int []parentsList(HashMap<NodeInterface, Integer> map) {
		int i = 0;
		for(G v : nodes) {
			map.put(v, i);
			++i;
		}
		int []parents = new int[nodeNum()];

		for(i = 0; i < parents.length; ++i) {
			parents[i] = i;
		}

		for(E e : edges) {
			int from = map.get(e.from);
			int to = map.get(e.to);

			union_find_merge(parents, from, to);
		}

		return parents;
	}

	/**
	 * Indicate this graph is connected
	 * @return this graph is connected
	 */
	public boolean isConnected() {
		HashMap<NodeInterface, Integer> map = new HashMap<>();

		int []parents = parentsList(map);
		int parent = union_find_lookup(parents, 0);
		union_find_flatten(parents, 0, parent);
		for(int i = 1; i < parents.length; ++i) {
			int t = union_find_lookup(parents, i);
			union_find_flatten(parents, i, t);
			if(parent != t) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Split this graph into connected ones
	 * @return splitted connected graphs from this graph
	 */
	public Graph<G, E>[] splitConnectedGraph() {
		HashMap<NodeInterface, Integer> map = new HashMap<>();

		int []parents = parentsList(map);
		int []numbers = new int[parents.length];
		int num = 0;
		for(int i = 0; i < parents.length; ++i) {
			if(i == parents[i]) {
				numbers[i] = num;
				++num;
			}
			else {
				int parent = union_find_lookup(parents, i);
				union_find_flatten(parents, i, parent);
				numbers[i] = numbers[parent];
			}
		}
		@SuppressWarnings("unchecked")
		Graph<G, E> []graphList = new Graph[num]; // This is okay, but not allowed in Java's type system
		for(int i = 0; i < num; ++i) {
			graphList[i] = new Graph<G, E>();
		}

		for(G v : nodes) {
			Graph<G, E> graph = graphList[numbers[map.get(v)]];
			graph.addNode(v);
		}

		for(E e : edges) {
			Graph<G, E> graph = graphList[numbers[map.get(e.from)]]; // e.from and e.to should be the same number
			graph.addEdge(e);
		}

		return graphList;
	}

	public G clicked(int x, int y) {
		for(G v : nodes) {
			if(v.inside(x,y)) {
				return v;
			}
		}
		return null;
	}

	public class AdjacentListGraph {
		final public int num;
		final public NodeInterface []V;
		final public int[][] E; //  "from" point to "E[from][i]"
		final public int[][] E_r; // "to" is pointed from "E[to][i]"
		final public HashMap<NodeInterface, Integer> map;
		AdjacentListGraph() {
			num = nodes.size();
			map = new HashMap<>();
			V = new NodeInterface[num];
			{
				int i = 0;
				for(G v : nodes) {
					map.put(v, i);
					V[i++] = v;
				}
			}
			E = new int[num][];
			E_r = new int[num][];
			int []nums = new int[num];
			int []nums_r = new int[num];
			for(int i = 0; i < num; ++i) {
				nums[i] = nums_r[i] = 0;
			}
			for(E e : edges) {
				nums[map.get(e.from)]++;
				nums_r[map.get(e.to)]++;
			}
			for(int i = 0; i < num; ++i) {
				E[i] = new int[nums[i]];
				E_r[i] = new int[nums_r[i]];
				nums[i] = nums_r[i] = 0;
			}
			for(E e : edges) {
				int i = map.get(e.from);
				int j = map.get(e.to);
				E[i][nums[i]++] = j;
				E_r[j][nums_r[j]++] = i;
			}
		}
	}

	public AdjacentListGraph adjacentlist() {
		return new AdjacentListGraph();
	}

	public class AdjacentMatrixGraph {
		final public int num;
		final public NodeInterface []V;
		final public boolean[][] E;
		final public HashMap<NodeInterface, Integer> map;
		AdjacentMatrixGraph() {
			num = nodes.size();
			map = new HashMap<>();
			V = new NodeInterface[num];
			{
				int i = 0;
				for(G v : nodes) {
					map.put(v, i);
					V[i++] = v;
				}
			}
			E = new boolean[num][num];
			for(int i = 0; i < num; ++i) {
				for(int j = 0; j < num; ++j) {
					E[i][j] = false;
				}
			}
			for(E e : edges) {
				E[map.get(e.from)][map.get(e.to)] = true;
			}
		}
	}

	public AdjacentMatrixGraph adjacentmatrix() {
		return new AdjacentMatrixGraph();
	}

	public static <G extends NodeInterface> Graph<G, Edge<G>> randomGenerate(final int rad, final int node_num, final int edge_num, final int width, final int height, Function<Integer, G> gen) {
		Random r = new Random();
		Graph<G, Edge<G>> x = new Graph<>();
		@SuppressWarnings("unchecked")
		G[] nodes = (G [])new NodeInterface[node_num]; // This is okay since we stored G's instances into nodes
		for(int i = 0; i < node_num; ++i) {
			nodes[i] = gen.apply(rad);
			x.addNode(nodes[i]);
		}
		for(int i = 1; i < node_num; ++i) {
			int to = r.nextInt(i);
			if(Math.random()<0.5) {
				x.addEdge(new Edge<G>(nodes[i], nodes[to]));
			}
			else {
				x.addEdge(new Edge<G>(nodes[to], nodes[i]));
			}
		}
		x.addEdge(new Edge<G>(nodes[0], nodes[r.nextInt(node_num-1)+1]));
		for(int i = node_num; i < edge_num; ++i) {
			int from = r.nextInt(node_num);
			int to;
			do {
				to = r.nextInt(node_num);
			} while(from == to);
			x.addEdge(new Edge<G>(nodes[from], nodes[to]));
		}
		x.randomPositions(rad, width-rad, rad, height-rad);
		return x;
	}

	public static <G extends NodeInterface> Graph<G, Edge<G>> randomDisconnectedGenerate(final int rad, final int graph_num, final int node_num, final int edge_num, final int width, final int height, Function<Integer, G> gen) {
		Graph<G, Edge<G>> g = randomGenerate(rad, node_num, edge_num, width, height, gen);
		for(int i = 1; i < graph_num; ++i) {
			Graph<G, Edge<G>> g2 = randomGenerate(rad, node_num, edge_num, width, height, gen);
			g.addAllNodes(g2.nodes);
			g.addAllEdges(g2.edges);
		}

		return g;
	}
}
