package info.chiguri.swing.plca;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.plca.DisplayConfig;
import info.chiguri.graphene.plca.Expression;
import info.chiguri.graphene.plca.Line;
import info.chiguri.graphene.plca.PLCAgenerator;
import info.chiguri.graphene.plca.PLCAviewer;
import info.chiguri.graphene.plca.Point;
import info.chiguri.swing.LeftDoubleClickedMouse;
import info.chiguri.swing.canvas.SwingColor;
import info.chiguri.swing.graphene.ClickedMoveMouse;
import info.chiguri.swing.graphene.GPanel;

public class GeneratorUI extends JPanel {
	static Graph<Point, Line> g;
	// 見た目パラメータ
	static final int RIGHT = 20, LEFT = 680, TOP = 20, BOTTOM = 680;

	public static void generateGraph() {
		g = new Graph<>();
		// 特にすることない？
	}

	public static void readGraph(InputStream input) {
		g = new Graph<>();
		BufferedReader in = new BufferedReader(new InputStreamReader(input));
		String str = "";
		int linenum = 0;
		try {
			int i = 0;
			++linenum;
			str = in.readLine();
			int nodenum = Integer.valueOf(str);
			if(nodenum < 0) {
				System.err.println("Number of nodes should be non-negative number");
				System.exit(1);
			}
			Point[] nodes = new Point[nodenum];
			for(i = 0; i < nodenum; ++i) {
				++linenum;
				str = in.readLine();
				String [] strs = str.split(" ");
				if(strs.length < 2) {
					throw new Exception();
				}
				int x = Integer.valueOf(strs[0]);
				int y = Integer.valueOf(strs[1]);
				nodes[i] = new Point(x, y);
				g.addNode(nodes[i]);
			}

			++linenum;
			str = in.readLine(); // 一行読み飛ばし

			// edgeの書式どうするかなあ・・・
			int edgenum = Integer.valueOf(str);

			for(i = 0; i < edgenum; ++i) {
				++linenum;
				str = in.readLine();
				String [] strs = str.split(" ");
				if(strs.length < 2) {
					throw new Exception();
				}
				int x = Integer.valueOf(strs[0]);
				int y = Integer.valueOf(strs[1]);
				if(x < 0 || x >= nodenum) {
					System.err.println("Node number is out of range : " + x);
					System.exit(1);
				}
				if(y < 0 || y >= nodenum) {
					System.err.println("Node number is out of range : " + y);
					System.exit(1);
				}
				if(x >= y) {
					System.err.println("This graph does not write both side : " + x + " " + y);
				}
				else {
					g.addEdge(new Line(nodes[x], nodes[y]));
					g.addEdge(new Line(nodes[y], nodes[x]));
				}
			}
		} catch (IOException e) {
			System.err.println("Format exception at line " + linenum + "  the last read line is : " + str);
			e.printStackTrace();
			System.exit(1);
		} catch (NumberFormatException e) {
			if(linenum <= 1) {
				System.err.println("The first line should be the number of nodes but given : " + str);
			}
			else {
				System.err.println("Pair of number required at line " + linenum + " but given : " + str);
			}
			e.printStackTrace();
			System.exit(1);
		} catch (Exception e) {
			System.err.println("Requires two numbers, but given : " + str);
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void printGraph(PrintStream out) {
		if(out == System.out || out == System.err) {
			out.println("The result of graph");
		}
		Point[] nodes = new Point[g.nodeNum()];
		//Iterator<Point> iv = g.nodeIterator();
		HashMap<Point, Integer> map = new HashMap<>();
		int i = 0;
		for(Point v : new IterableIterator<>(g.nodeIterator())) {
			map.put(v, i);
			nodes[i] = v;
			++i;
		}

		out.println(g.nodeNum());
		for(i = 0; i < g.nodeNum(); ++i) {
			out.println((int)(nodes[i].getPoint().x) + " " + (int)(nodes[i].getPoint().y));
		}
		out.println(g.edgeNum()/2); // 一方しか使わない
		for(Line l : new IterableIterator<>(g.edgeIterator())) {
			int i1 = map.get(l.from);
			int i2 = map.get(l.to);
			if(i1 < i2) {
				out.println(map.get(l.from) + " " + map.get(l.to));
			}
		}
	}

	public static void initDisplayConfig() {
		DisplayConfig.PointRadius = 10;
		DisplayConfig.SlidedTangent = 1.0/8.0;

		DisplayConfig.NotSelectedPointColor = new SwingColor(Color.yellow);
		DisplayConfig.NotSelectedPointEdgeColor = new SwingColor(Color.black);
		DisplayConfig.SelectedPointColor = new SwingColor(Color.magenta);
		DisplayConfig.SelectedPointEdgeColor = new SwingColor(Color.red);

		DisplayConfig.NotSelectedEdgeColor = new SwingColor(Color.black);
		DisplayConfig.SelectedEdgeColor = new SwingColor(Color.red);

		DisplayConfig.SelectedEdgeWidth = 3.0f;
	}

	public static void main(String[] args) {
		if(args.length < 1) {
			generateGraph();
		}
		else {
			if(args[0].equals("-i")) {
				readGraph(System.in);
			}
			else {
				try {
					readGraph(new FileInputStream(args[0]));
				} catch (FileNotFoundException e) {
					System.err.println("The file " + args[0] + " cannot be opened");
					e.printStackTrace();
					System.exit(1);
				}
			}
		}

		initDisplayConfig();

		final JFrame m = new JFrame();

		m.setSize(LEFT+RIGHT, BOTTOM+TOP);
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				printGraph(System.out);
			}
		});
		GPanel<Point, Line> panel = new GPanel<>(g);
		panel.addMouseAdapter(new LeftDoubleClickedMouse(e -> {
			String filename = Long.toString(System.currentTimeMillis());
			try {
				printGraph(new PrintStream(filename+"-GRAPH.txt"));
				System.out.println("Save Text Graph representaiton as " + filename + "-GRAPH.txt");
			} catch (FileNotFoundException exc) {
				exc.printStackTrace();
			}
			try {
				Expression exp = new PLCAgenerator(g).generatePLCA();
				if(exp != null) {
					exp.printPLCA(new PrintStream(filename+"-PLCA.txt"));
					System.out.println("Save PLCA expression as " + filename + "-PLCA.txt");
					PLCAviewer.displayExpression(exp, new JFrame());
				}
			} catch (FileNotFoundException exc) {
				exc.printStackTrace();
			} catch (RuntimeException exc) {
				exc.printStackTrace(); // 仮置き
			}
		}));
		panel.addMouseAdapter(new ClickedMoveMouse<>(g));
		panel.addMouseAdapter(new MouseAdapter() {
			Point clicked;
			@Override
			public void mousePressed(MouseEvent e) {
				// this listener is used for right button, but not left/middle button
				if(!SwingUtilities.isRightMouseButton(e)) {
					return;
				}
				Point previous = clicked;
				if(previous != null) {
					previous.unfocused();
				}

				int x = e.getX();
				int y = e.getY();
				clicked = g.clicked(x, y);

				// adding node (by double clicking)
				if(clicked == null && e.getClickCount() == 2 && !e.isConsumed()) {
					g.addNode(new Point(x, y));
					return;
				}
				// removing node (by double clicking)
				if(clicked != null && e.getClickCount() == 2 && !e.isConsumed() && clicked == previous) {
					g.removeNode(clicked);
					clicked = null;
					return;
				}

				// right clicking "from" and then "to" generates/removes arc from "from" and "to"
				if(clicked != null && previous != null && clicked != previous) {
					Line arc = new Line(previous, clicked);
					Line inv = new Line(clicked, previous);
					if(g.containsEdge(arc)) {
						g.removeEdge(arc);
						g.removeEdge(inv);
					}
					else {
						g.addEdge(arc);
						g.addEdge(inv);
					}
					clicked = null; // to avoid mis-directing from current "clicked" to future "clicked"
				}
				if(clicked != null) {
					clicked.focused();
				}
			}
		});
		m.add(panel);
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 100, 100);
	}
}
