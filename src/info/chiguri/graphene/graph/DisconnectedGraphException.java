package info.chiguri.graphene.graph;

public class DisconnectedGraphException extends RuntimeException {
	public <G extends NodeInterface, E extends Edge<G>> DisconnectedGraphException(Graph<G, E> g) {
		super(g.toString() + " is not a connected graph"); // maybe unreadable message (because toString method is inherited from Object)
	}
}
