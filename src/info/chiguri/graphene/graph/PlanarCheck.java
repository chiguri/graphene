package info.chiguri.graphene.graph;

//This class gives methods to judge "a graph is planar", but not "drawn as plane graph" (cf. PlaneCheck)
// This class uses PlanarRepositioning to determine whether a graph can be embedded into a plane
public class PlanarCheck {
	static public <G extends NodeInterface, E extends Edge<G>> boolean isPlanar(Graph<G, E> g) {
		Graph<CircleNode, Edge<CircleNode>> graph = new Graph<>();
		g.cloning(graph, p -> new CircleNode(1, p.getPoint()), (map, e) -> new Edge<>(map.get(e.from), map.get(e.to)));

		try {
			for(Graph<CircleNode, Edge<CircleNode>> sub : graph.splitConnectedGraph()) {
				new PlanarRepositioning<>(sub).step();
			}
		} catch(NonPlanarGraphException e) {
			//e.printStackTrace();
			return false;
		}
		return true;
	}

	static public <G extends NodeInterface, E extends Edge<G>> boolean planarityTest(Graph<G, E> g) {

		return true;
	}
}
