package info.chiguri.graphene.plca;

import info.chiguri.graphene.canvas.AbstractColor;

public final class DisplayConfig {
	public static int PointRadius = 10;
	public static double SlidedTangent = 1.0/8.0;

	public static AbstractColor NotSelectedPointColor = null;
	public static AbstractColor NotSelectedPointEdgeColor = null;
	public static AbstractColor SelectedPointColor = null;
	public static AbstractColor SelectedPointEdgeColor = null;

	public static AbstractColor NotSelectedEdgeColor = null;
	public static AbstractColor SelectedEdgeColor = null;

	public static float SelectedEdgeWidth = 3.0f;
}
