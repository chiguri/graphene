package info.chiguri.swing.graphene;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.swing.SwingUtilities;

import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.NodeInterface;
import info.chiguri.graphene.graph.Point;

public class ModifyGraphMouse<G extends NodeInterface, E extends Edge<G>> extends MouseAdapter {
	protected Graph<G, E> graph;
	protected Function<Point, G> generator;
	protected BiFunction<G, G, E> edgeGenerator;
	protected G clicked;

	public ModifyGraphMouse(Graph<G, E> g, Function<Point, G> gen, BiFunction<G, G, E> egen) {
		graph = g;
		generator = gen;
		edgeGenerator = egen;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// this listener is used for right button, but not left/middle button
		if(!SwingUtilities.isRightMouseButton(e)) {
			return;
		}
		G previous = clicked;

		int x = e.getX();
		int y = e.getY();
		clicked = graph.clicked(x, y);

		// adding node (by double clicking)
		if(clicked == null && e.getClickCount() == 2 && !e.isConsumed()) {
			if(generator != null) {
				graph.addNode(generator.apply(new Point(x, y)));
			}
			else {
				System.out.println("Generator is not set properly");
			}
		}

		// right clicking "from" and then "to" generates/removes arc from "from" and "to"
		if(clicked != null && previous != null && clicked != previous) {
			E arc = edgeGenerator.apply(previous, clicked);
			if(graph.containsEdge(arc)) {
				graph.removeEdge(arc);
			}
			else {
				graph.addEdge(arc);
			}
			clicked = null; // to avoid mis-directing from current "clicked" to future "clicked"
		}
	}
}
