package info.chiguri.graphene.argumentation;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.graph.Edge;

public class Attack extends Edge<Argument> {
	private static Pattern attackPattern = Pattern.compile("^\\[(.+)-->(.+)\\]");

	public Attack(Argument v1, Argument v2) {
		super(v1, v2);
	}

	@Override
	public void drawEdge(AbstractCanvas canvas, boolean bothside) {
		switch (from.getState()) {
		case in:
			canvas.setColor(DisplayConfig.ValidAttackColor);
			break;

		case out:
			canvas.setColor(DisplayConfig.InValidAttackColor);
			break;

		case undec:
			canvas.setColor(DisplayConfig.UnclearAttackColor);
			break;

		default:
			throw new RuntimeException("Argument state is not initialized : " + from);
		}

		super.drawEdge(canvas, bothside);
	}

	@Override
	public String toString() {
		return "[" + from + "-->" + to + "]";
	}

	public static Attack parseAttack(String str, Map<String, Argument> map) {
		Matcher match = attackPattern.matcher(str);
		if(match.find()) {
			Argument from = map.get(match.group(1));
			Argument to = map.get(match.group(2));
			if(from != null && to != null) {
				return new Attack(from, to);
			}
		}
		return null; // throw?
	}

}
