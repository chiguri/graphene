package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;

/**
 * @author chiguri
 * Base class for implementing NodeInterface
 */
public abstract class AbstractNode implements NodeInterface {
	protected Point p;

	/**
	 * Constructor setting node's center as (0,0)
	 */
	AbstractNode() {
		p = new Point();
	}

	/**
	 * @param x : the x-coordinate of the center of the node
	 * @param y : the y-coordinate of the center of the node
	 */
	public AbstractNode(int x, int y) {
		p = new Point(x, y);
	}

	/**
	 * @param p : the center position of the node
	 */
	public AbstractNode(Point p) {
		this.p = p;
	}

	@Override
	public boolean equals(Object o) {
		return this == o; // in this setting, Node checks only "the same object" but does not check the data (such as p)
	}

	public abstract boolean inside(int x, int y);

	public void moveTo(Point p) {
		this.p = p;
	}

	abstract public Point getEdgePoint(Point p);

	public Point getPoint() {
		return p;
	}

	abstract public void draw(AbstractCanvas canvas);
}
