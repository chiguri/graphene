package info.chiguri.graphene;

import java.util.Iterator;

public class IterableIterator<T> implements Iterable<T>, Iterator<T> {
	Iterator<T> iter;

	public IterableIterator(Iterator<T> iter) {
		this.iter = iter;
	}

	@Override
	public Iterator<T> iterator() {
		return iter;
	}

	@Override
	public boolean hasNext() {
		// TODO 自動生成されたメソッド・スタブ
		return iter.hasNext();
	}

	@Override
	public T next() {
		// TODO 自動生成されたメソッド・スタブ
		return iter.next();
	}

}
