package info.chiguri.graphene.plca;

import java.util.Iterator;

public class Segment implements PLCAobject, Iterable<Line> {
	final protected Line[] seg; // Do not modify after initialization
	final public Point start, end;

	public Segment(Point p) {
		start = end = p;
		seg = new Line[0];
	}

	public Segment(Line[] lines) {
		seg = lines;
		if(lines.length == 0) {
			throw new RuntimeException("Segment except for point-only segment should consist from more than one lines");
		}
		start = lines[0].from;
		end = lines[lines.length-1].to;
		checkSequence();
	}

	public Segment(Point s, Line[] lines, Point e) {
		start = s; end = e;
		seg = lines;
		checkSequence();
	}

	private void checkSequence() {
		Point prev = start;
		for(Line l : seg) {
			if(prev != l.from) {
				throw new RuntimeException("Segment should constist from sequencing lines");
			}
			prev = l.to;
		}
		if(prev != end) {
			throw new RuntimeException("Segment should constist with sequencing lines");
		}
	}

	public int length() {
		return seg.length;
	}

	public Line get(int i) {
		return seg[i];
	}

	@Override
	public void focused() {
		start.focused();
		for(Line l : seg) {
			l.focused();
		}
	}

	@Override
	public void unfocused() {
		start.unfocused();
		for(Line l : seg) {
			l.unfocused();
		}
	}

	class SegmentIterator implements Iterator<Line>, Iterable<Line> {
		private int current;
		SegmentIterator() {
			current = 0;
		}
		SegmentIterator(int start) {
			current = start;
		}
		@Override
		public boolean hasNext() {
			return current <= seg.length-1;
		}

		@Override
		public Line next() {
			return seg[current++];
		}
		@Override
		public Iterator<Line> iterator() {
			return this;
		}
	}

	@Override
	public Iterator<Line> iterator() {
		return new SegmentIterator();
	}

	public SegmentIterator iterateFrom(int start) {
		return new SegmentIterator(start);
	}
}

