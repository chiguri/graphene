package info.chiguri.graphene.plca;

import info.chiguri.graphene.canvas.AbstractCanvas;
import info.chiguri.graphene.graph.CircleNode;

public class Point extends CircleNode implements PLCAobject {
	protected boolean selected = false;

	public Point(int x, int y) {
		super(DisplayConfig.PointRadius, x, y); // radius is fixed
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		if(selected) {
			canvas.setColor(DisplayConfig.SelectedPointColor);
		}
		else {
			canvas.setColor(DisplayConfig.NotSelectedPointColor);
		}
		canvas.fillOval(p.x-r, p.y-r, r*2, r*2);

		if(selected) {
			canvas.setColor(DisplayConfig.SelectedPointEdgeColor);
		}
		else {
			canvas.setColor(DisplayConfig.NotSelectedEdgeColor);
		}
		canvas.drawOval(p.x-r, p.y-r, r*2, r*2);
		// draw id?
	}

	@Override
	public void focused() {
		selected = true;
	}

	@Override
	public void unfocused() {
		selected = false;
	}
}
