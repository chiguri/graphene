package info.chiguri.swing.graphene;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Function;

import javax.swing.JFrame;

import info.chiguri.graphene.graph.AbstractRepositioning;
import info.chiguri.graphene.graph.CircleStringNode;
import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.NodeInterface;
import info.chiguri.graphene.graph.OrderedRepositioning;
import info.chiguri.graphene.graph.SimpleRepositioning;
import info.chiguri.graphene.graph.SquareNode;
import info.chiguri.graphene.graph.ToggleCircleNode;
import info.chiguri.graphene.graph.ToggleNodeInterface;
import info.chiguri.swing.canvas.SwingCanvas;
import info.chiguri.swing.canvas.SwingColor;

public class GPanelSample {
	public static void main(String[] args) {
		test3();
	}

	static class Generator1 implements Function<Integer, NodeInterface> {
		int i = 0;
		public NodeInterface apply(Integer rad) {
			if(i < 10) return new CircleStringNode(""+(i++), rad, rad, 0, 0);
			else return new SquareNode(rad, 0, 0);
		}
	}

	public static void test0() {
		final Graph<NodeInterface, Edge<NodeInterface>> g = Graph.randomGenerate(10, 20, 35, 700, 700, new Generator1());
		g.normalize(20, 680, 20, 680);
		final JFrame m = new JFrame();
		final AbstractRepositioning<NodeInterface, Edge<NodeInterface>> rep = new SimpleRepositioning<>(g, 20, 100, 1000000.0, 0.1, 0.8, 0.05, 1.0);

		m.setSize(800, 800);
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.add(new GPanel<NodeInterface, Edge<NodeInterface>>(g));
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				rep.step();
				double ratio = g.normalize(20, 680, 20, 680);
				rep.enlarge(ratio);
				m.repaint();
				System.err.println("how about?");
			}
		}, 2000, 100);
	}

	public static void test1() {
		final int initSize = 1000;
		final int numCluster = 25;
		final SwingColor red = new SwingColor(Color.red);
		final SwingColor blue = new SwingColor(Color.blue);

		final Graph<ToggleNodeInterface, Edge<ToggleNodeInterface>> graph = Graph.randomDisconnectedGenerate(20/(int)(Math.sqrt(numCluster-1)+1), numCluster, 20, 35, initSize, initSize, ((r)->{ return new ToggleCircleNode(r.intValue(), red, blue, 0, 0); }));
		final Graph<ToggleNodeInterface, Edge<ToggleNodeInterface>>[] split = graph.splitConnectedGraph();
		int s = (int)(Math.sqrt(split.length-1.0))+1;
		System.out.println(split.length + " graphs found");
		final JFrame m = new JFrame();
		m.setSize(initSize, initSize+50); // used as toolbar
		for (int i = 0; i < split.length; ++i) {
			final int col = i%s;
			final int row = i/s;
			final Graph<ToggleNodeInterface, Edge<ToggleNodeInterface>> g = split[i];
			final AbstractRepositioning<ToggleNodeInterface, Edge<ToggleNodeInterface>> rep = new SimpleRepositioning<>(g, 20, 100, 1000000.0, 0.1, 0.8, 0.05, 1.0);

			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					for (int i = 0; i < 500; ++i)
						rep.step();
					int width = m.getWidth()-50;
					int height = m.getHeight()-50;
					double ratio = g.normalize(width*col/s+20, width*(col+1)/s-20, height*row/s+20, height*(row+1)/s-20);
					rep.enlarge(ratio);
				}
			}, 2000, 100);
		}

		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MouseAdapter ad = new ClickedMoveMouse<ToggleNodeInterface, Edge<ToggleNodeInterface>>(graph) {
			public void mousePressed(MouseEvent e) {
				if(clicked != null) {
					clicked.setMode(false);
				}
				clicked = graph.clicked(e.getX(), e.getY());
				if(clicked != null) {
					clicked.setMode(true);
				}
			}
		};
		GPanel<ToggleNodeInterface, Edge<ToggleNodeInterface>> panel = new GPanel<ToggleNodeInterface, Edge<ToggleNodeInterface>>(graph) {
			@Override
			public void paintComponent(Graphics graphics) {
				super.paintComponent(graphics);
				for(int i = 0; i < split.length; ++i) {
					split[i].drawWindow(new SwingCanvas((Graphics2D)graphics));
				}
			}
		};
		panel.addMouseAdapter(ad);
		m.add(panel);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 2000, 100);
		m.setVisible(true);
	}

	public static void test2() {
		final Graph<NodeInterface, Edge<NodeInterface>> graph = Graph.randomDisconnectedGenerate(10, 5, 20, 35, 700, 700, new Generator1());
		graph.normalize(20, 680, 20, 680);
		Graph<NodeInterface, Edge<NodeInterface>>[] split = graph.splitConnectedGraph();
		System.out.println(split.length + " graphs found");
		for (int i = 0; i < split.length; ++i) {
			final Graph<NodeInterface, Edge<NodeInterface>> g = split[i];
			final JFrame m = new JFrame();
			final AbstractRepositioning<NodeInterface, Edge<NodeInterface>> rep = new SimpleRepositioning<>(g, 20, 100, 1000000.0, 0.1, 0.8, 0.05, 1.0);

			m.setSize(800, 800);
			m.setLocation(i*50+100, i*50);
			m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			m.add(new GPanel<NodeInterface, Edge<NodeInterface>>(g));
			m.setVisible(true);
			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					for (int i = 0; i < 500; ++i)
						rep.step();
					double ratio = g.normalize(20, 680, 20, 680);
					rep.enlarge(ratio);
					m.repaint();
				}
			}, 2000, 100);
		}
		final JFrame m = new JFrame();

		m.setSize(800, 800);
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.add(new GPanel<NodeInterface, Edge<NodeInterface>>(graph));
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				m.repaint();
			}
		}, 2000, 100);
	}

	public static void test3() {
		final Graph<NodeInterface, Edge<NodeInterface>> graph = Graph.randomGenerate(10, 20, 20, 700, 700, new Generator1());
		graph.normalize(20, 680, 20, 680);
		final JFrame m = new JFrame();
		final AbstractRepositioning<NodeInterface, Edge<NodeInterface>> rep = new OrderedRepositioning<>(graph, 0, 700, 0, 700);

		m.setSize(800, 800);
		m.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		m.add(new GPanel<NodeInterface, Edge<NodeInterface>>(graph));
		m.setVisible(true);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				if(!rep.finish()) {
					rep.step();
				}
				m.repaint();
			}
		}, 2000, 100);
	}
}
