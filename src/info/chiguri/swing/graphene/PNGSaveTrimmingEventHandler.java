package info.chiguri.swing.graphene;

import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.graph.Edge;
import info.chiguri.graphene.graph.Graph;
import info.chiguri.graphene.graph.NodeInterface;
import info.chiguri.graphene.graph.Point;
import info.chiguri.swing.PNGSaveEventHandler;

public class PNGSaveTrimmingEventHandler<G extends NodeInterface, E extends Edge<G>> extends PNGSaveEventHandler {
	protected Graph<G, E> graph;
	protected int margin;

	public PNGSaveTrimmingEventHandler(JComponent j, Graph<G, E> g, int margin) {
		super(j);
		graph = g;
		this.margin = margin;
	}

	// Trimming is based on the node size, rather than drawn line (so segment can be processed properly, but Bazier-curve may be cut)
	@Override
	public void accept(MouseEvent e) {
		BufferedImage img = new BufferedImage(renderer.getWidth(), renderer.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		renderer.paint(img.createGraphics());

		int left = renderer.getWidth(), right = 0, top = renderer.getHeight(), bottom = 0;
		for(G v : new IterableIterator<>(graph.nodeIterator())) {
			Point p = v.getPoint();
			if(p.x < left)   left   = (int)p.x;
			if(p.x > right)  right  = (int)p.x;
			if(p.y < top)    top    = (int)p.y;
			if(p.y > bottom) bottom = (int)p.y;
		}
		if(left > right) left = right;
		if(top > bottom) top = bottom;

		if(left-margin < 0) left = 0;
		else left -= margin;
		if(right+margin > renderer.getWidth()) right = renderer.getWidth();
		else right += margin;
		if(top-margin < 0) top = 0;
		else top -= margin;
		if(bottom+margin > renderer.getHeight()) right = renderer.getHeight();
		else bottom += margin;

		img = img.getSubimage(left, top, right-left, bottom-top);
		String filename = Long.toString(System.currentTimeMillis());
		saveImage(filename, img);
	}
}
