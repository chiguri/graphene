package info.chiguri.graphene.graph;

import info.chiguri.graphene.canvas.AbstractCanvas;

public class SquareNode extends AbstractNode {
	protected int s;

	SquareNode() {
		s = 2;
	}
	public SquareNode(int s, Point p) {
		super(p);
		this.s = s;
	}
	public SquareNode(int s, int x, int y) {
		super(x, y);
		this.s = s;
	}

	@Override
	public boolean inside(int x, int y) {
		return (x >= p.x-s && x <= p.x+s && y >= p.y-s && y <= p.y+s);
	}

	@Override
	public Point getEdgePoint(Point p) {
		if(this.p.equals(p)) return p;
		double dx = p.x - this.p.x;
		double dy = p.y - this.p.y;
		double d = Math.sqrt(dx*dx+dy*dy);
		double sine = dy/d;
		double cosine = dx/d;
		int x = this.p.x;
		int y = this.p.y;
		if(Math.abs(sine) > Math.abs(cosine)) {
			if(sine < 0) {
				y -= s;
			}
			else {
				y += s;
			}
			x += s * cosine;
		}
		else {
			if(cosine < 0) {
				x -= s;
			}
			else {
				x += s;
			}
			y += s * sine;

		}
		return new Point(x, y);
	}

	@Override
	public void draw(AbstractCanvas canvas) {
		canvas.drawRect((int)(p.x-s), (int)(p.y-s), s*2, s*2);
	}

}
