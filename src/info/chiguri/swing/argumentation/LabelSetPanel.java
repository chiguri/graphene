package info.chiguri.swing.argumentation;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import info.chiguri.graphene.IterableIterator;
import info.chiguri.graphene.argumentation.Argument;
import info.chiguri.graphene.argumentation.ArgumentationFramework;
import info.chiguri.graphene.argumentation.Label;

public class LabelSetPanel extends JPanel {
	protected final ArgumentationFramework AF;
	protected final Argument[] list;
	protected final Label[] labels;

	public LabelSetPanel(ArgumentationFramework AF) {
		super();
		this.AF = AF;

		list = new Argument[AF.nodeNum()];
		labels = new Label[AF.nodeNum()];
		{
			int i = 0;
			for(Argument a : new IterableIterator<>(AF.nodeIterator())) {
				labels[i] = null;
				list[i++] = a;
			}
		}

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel top = new JPanel();
		top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
		top.add(new JLabel("Init Label Mapping"));
		JButton finish = new JButton("Calculate");
		top.add(finish);

		add(top);

		finish.addActionListener((e) -> {
			Map<Argument, Label> map = AF.completeLabeling(currentMap());
			for(Map.Entry<Argument, Label> entry : map.entrySet()) {
				System.out.println(entry);
				entry.getKey().setLabel(entry.getValue());
			}
		});
		for(int i = 0; i < list.length; ++i) {
			final JPanel p = new JPanel();
			p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));

			final ButtonGroup bg = new ButtonGroup();
			final JRadioButton buttonIn = new JRadioButton("in");
			final JRadioButton buttonUndec = new JRadioButton("undec");
			final JRadioButton buttonOut = new JRadioButton("out");
			final JRadioButton buttonUnknown = new JRadioButton("unset");
			bg.add(buttonIn);
			bg.add(buttonUndec);
			bg.add(buttonOut);
			bg.add(buttonUnknown);

			final int num = i;
			buttonIn.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						labels[num] = Label.in;
					}
				}
			});
			buttonUndec.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						labels[num] = Label.undec;
					}
				}
			});
			buttonOut.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						labels[num] = Label.out;
					}
				}
			});
			buttonUnknown.addItemListener(new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(e.getStateChange() == ItemEvent.SELECTED) {
						labels[num] = null;
					}
				}
			});

			buttonUnknown.setSelected(true);

			p.add(new JLabel(list[i].toString()));
			p.add(buttonIn);
			p.add(buttonUndec);
			p.add(buttonOut);
			p.add(buttonUnknown);

			add(p);
		}
	}


	private Map<Argument, Label> currentMap() {
		Map<Argument, Label> map = new HashMap<>();
		for(int i = 0; i < list.length; ++i) {
			if(labels[i] != null) {
				map.put(list[i], labels[i]);
			}
		}
		return map;
	}
	// nullとLabelで設定の有無？

}
